<?php

namespace Tests;

use Illuminate\Contracts\Console\Kernel;

trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        // self:: initialize();
        return $app;
    }


    public static function initialize()
    {

        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        if(true){
            // dd(config('database.default'));
            // if (config('database.default') == 'sqlite') {
                $db = app()->make('db');

                dd($db->connection()->getPdo());
                $db->connection()->getPdo()->exec("pragma foreign_keys=1");
            // }
    
            \Artisan::call('migrate');
            \Artisan::call('db:seed');
    
            //self::$configurationApp = $app;
            return $app;
        }
    
        return self::$configurationApp;
    }
    
}
