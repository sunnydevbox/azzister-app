<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class APIExistsTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAPIRouteStatus400()
    {
        $response = $this->get('/api');

        $response->assertStatus(400);
    }

    public function testAPIRouteStatus200()
    {
        $response = $this->get('/api');

        $response->assertStatus(400);
    }
}
