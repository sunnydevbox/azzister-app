<?php

use Illuminate\Database\Seeder;
use \Azzister\Account\Models\User;
use \Azzister\Account\Models\Application;
use \Azzister\Account\Models\Business;

class FreshDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'email' => 'admin@admin.com',
            'password'  => \Hash::make('1234qwer'),
        ]);

        $business1 = Business::create([
            'name'      => 'Carl',
            'description'   => '1st client'
        ]);

        $app1 = Application::create([
            'name'      => 'Employee Records',
            'description'      => 'Employee Records',
            'version'   => '1',
            'is_enabled' => true,
        ]);

        $app2 = Application::create([
            'name'      => 'Timelogger',
            'description'      => 'Employee Records',
            'version'   => '0.1',
            'is_enabled' => true,
        ]);

    
        
        $business1->applications()->save($app1, [
            'app_code' => '1_1',
            'status'    => 'ACTIVE',
            'is_enabled'    => true
        ]);
        $business1->applications()->save($app2, [
            'app_code' => '1_2',
            'status'    => 'ACTIVE',
            'is_enabled'    => true
        ]);
        // $business1->applications()->save($app2);

        
        
    }
}
