<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name');
            $table->text('description');
            $table->timestamps();
            $table->softDeletes();
        });


        Schema::create('businesses_applications', function (Blueprint $table) {
            
            $table->bigInteger('business_id')->unsigned();
            $table->bigInteger('application_id')->unsigned();

            $table->string('app_code')->unique();
            $table->boolean('is_enabled')->default(false);
            $table->string('status');

            $table->foreign('business_id')
                ->references('id')
                ->on('businesses')
                ->onDelete('cascade');

            $table->foreign('application_id')
                ->references('id')
                ->on('applications')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('businesses_applications', function($table) {
            $table->dropForeign('businesses_applications_business_id_foreign');
            
            $table->dropForeign('businesses_applications_application_id_foreign');
        });
        Schema::dropIfExists('businesses_applications');
        Schema::dropIfExists('businesses');
    }
}
