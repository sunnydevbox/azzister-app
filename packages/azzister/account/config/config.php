<?php

return [

    /*****************
     *** MODELS
     *****************/
    'model_account'                 => \Azzister\Account\Models\Account::class,
    'model_application'             => \Azzister\Account\Models\Application::class,
    'model_business'                => \Azzister\Account\Models\Business::class,
    'model_business_application'    => \Azzister\Account\Models\BusinessApplication::class,
];