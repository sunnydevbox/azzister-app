<?php
namespace Azzister\Account;

use Illuminate\Support\ServiceProvider;

class AzzisterAppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application events.
     */
    public function boot()
    {

        $this->loadRoutesFrom(__DIR__.'/../routes/api.php');
        
        $this->publishes([
            __DIR__.'/../config/config.php' => config_path('azzister-app.php'),
        ], 'config');

        $this->mergeConfigFrom(
            __DIR__ . '/../config/config.php', 'azzister-app'
        );


        //$this->loadMigrationsFrom(__DIR__.'/..//database/migrations');
        // if ($this->app->runningInConsole()) {
        //     $this->commands([
        //         \Sunnydevbox\TWUser\Console\Commands\PublishMigrationsCommand::class,
        //         \Sunnydevbox\TWUser\Console\Commands\PublishConfigCommand::class,
        //         \Sunnydevbox\TWUser\Console\Commands\SetupCommand::class,
        //     ]);
        // }
    }
}