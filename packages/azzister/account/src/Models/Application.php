<?php
namespace Azzister\Account\Models;


class Application extends \Sunnydevbox\TWCore\Models\BaseModel
{
    protected $table = 'applications';

    public function businesses()
    {
        return $this->hasManyThrough(config('azzister-app:model_business_app'), 'business_id', 'id');
    }
}