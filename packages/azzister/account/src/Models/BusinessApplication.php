<?php
namespace Azzister\Account\Models;


class BusinessApplication extends \Sunnydevbox\TWCore\Models\BaseModel
{
    protected $table = 'businesses_applications';

    public function applications()
    {
        return $this->belongsToMany(config('azzister-account:model_application'),  'application_id');
    }

    public function businesses()
    {
        return $this->belongsToMany(config('azzister-account:model_business'), 'business_id');
    }
    
}