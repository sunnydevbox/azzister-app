<?php
namespace Azzister\Account\Models;


class Business extends \Sunnydevbox\TWCore\Models\BaseModel
{
    protected $table = 'businesses';

    public function applications()
    {   
        return $this->belongsToMany(
            config('azzister-app.model_application'),
            'businesses_applications'
        );
    }
}